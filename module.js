"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var DBConnector = /** @class */ (function () {
    function DBConnector(credentials) {
        var _this = this;
        this.connect = function () {
            var url = "mongodb://"
                + _this.credentials.DATABASE_USER + ":"
                + _this.credentials.DATABASE_PASSWORD + "@"
                + _this.credentials.DATABASE_HOST + "/" + _this.credentials.DATABASE_COLLECTION
                + "?qa.connectionTimeout=30&qa.maxIdleTimeout=0";
            mongoose_1.connect(url, { useNewUrlParser: true }, function (err) {
                if (err) {
                    console.log(err);
                    throw err;
                }
            }).catch(function (err) { throw err; });
        };
        this.disconnect = function () {
            mongoose_1.connection.close();
        };
        this.credentials = credentials;
    }
    return DBConnector;
}());
exports.DBConnector = DBConnector;
