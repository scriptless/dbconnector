interface IDBCredentials {
    DATABASE_USER: string;
    DATABASE_PASSWORD: string;
    DATABASE_HOST: string;
    DATABASE_COLLECTION: string;
}
declare class DBConnector {
    private credentials;
    constructor(credentials: IDBCredentials);
    connect: () => void;
    disconnect: () => void;
}
export { DBConnector, IDBCredentials };
